//Este mensaje se muestra en la consola del navegador
console.log("Hola mundo :)" );

//este mensaje es una ventana emergente del navegador
alert("Hola usuarios");

//este mensaje se imprime dentro del html de mi sitio
document.write("Hola Mundo ;)")

//Declaracion de variable
let nombre = "pablo"

document.write("Hola " + nombre);

//declaracion de variables let - var(forma antigua) - const

var edad=31;
let age=30;

document.write("<br> Variable var let");
document.write("<br>Edad= "+edad);
document.write("<br>Age= "+age);

edad=25;
age=30;

document.write("<br>Modifique el valor de edad y age);
document.write("<br>Edad= "+edad);
document.write("<br>Age= "+age);

const url="https://google.com";
document.write("<br>URL="+url);

//url="Algun texto";
//document.write("<br>URL="+url);

let contador=1;
document.write("<br>Contador= "+contador);
contador++; //es lo mismo que poner contador=contador+1
document.write("<br>Contador= "+contador);